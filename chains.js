var chainlist = [
    {
        accountMode: 'permissionedWithCert',
        chainId: 'chainmaker_testnet_chain',
        chainName: '开放测试网络 (cert)',
        description: '长安链开放测试网络（Cert）是长安链官方推出的账户模式为证书账户的测试网络，供社区用户进行区块链应用开发测试。',
        hostName: 'chainmaker.org',
        logo: 'https://git.chainmaker.org.cn/chainmaker/issue/-/raw/master/images/logo%E8%8B%B1%E6%96%87.png?inline=false',
        rpcs: [
            {
                url: '152.136.217.46:12302',
                orgName: "长安链官方"
            },
            {
                url: '49.232.86.161:12302',
                orgName: "长安链官方"
            },
            {
                url: '82.157.120.56:12302',
                orgName: "长安链官方"
            },
            {
                url: '152.136.210.129:12302',
                orgName: "长安链官方"
            }
        ],
        tlsEnable: true,
        browserLink: 'https://explorer-testnet.chainmaker.org.cn/',
    },
];
